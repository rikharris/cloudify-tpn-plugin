# Copyright 2018 Telstra Open Source
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


# ctx is imported and used in operations
# from cloudify import ctx

# put the operation decorator on any function that is a task
from cloudify.decorators import operation

import TelstraTPN
from TelstraTPN.rest import ApiException
from pprint import pprint
from cloudify.exceptions import NonRecoverableError


def get_param(ctx, kwargs, paramName):
    param = kwargs.get(paramName)
    if not param:
        param = ctx.instance.runtime_properties.get(paramName)

    if not param:
        param = ctx.node.properties.get(paramName)

    ctx.instance.runtime_properties[paramName] = param


def get_params(ctx, kwargs):
    tpn_username = get_param(ctx, kwargs, 'tpn_username')
    tpn_password = get_param(ctx, kwargs, 'tpn_password')
    return({'tpn_username': tpn_username, 'tpn_password': tpn_password})


def get_token(params):
    api_instance = TelstraTPN.AuthenticationApi()
    grant_type = 'password'
    username = params['tpn_username']
    password = params['tpn_password']

    try:
        # Create an authentication token
        api_response = api_instance.generate_token(
            grant_type, username, password)
        pprint(api_response)
    except ApiException as e:
        print(
            "Exception when calling AuthenticationApi->generate_token: %s\n"
            % e)
    return api_response.access_token


@operation
def create(ctx, **kwargs):
    p = get_params(ctx, kwargs)
    access_token = get_token(p)
    raise NonRecoverableError('(create) token: %s' % access_token)
    pass


@operation
def delete(ctx, **kwargs):
    p = get_params(ctx, kwargs)
    access_token = get_token(p)
    raise NonRecoverableError('(delete) token: %s' % access_token)
    pass
