from setuptools import setup

setup(
    name='cloudify-tpn-plugin',
    version='1.0',
    author='Telstra',
    packages=['cloudify_tpn'],
    dependency_links=['https://github.com/Telstra/Programmable-Network-SDK-python.git'],
    install_requires=['cloudify-plugins-common>=3.4'],
)
