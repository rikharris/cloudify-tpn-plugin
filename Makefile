.PHONY: docker-build build test package zip
T := cloudify-tpn-plugin

docker/dev-requirements.txt: dev-requirements.txt
	cp -i dev-requirements.txt docker/dev-requirements.txt

docker-build: docker/dev-requirements.txt
	docker build -t cloudify-tpn-plugin docker

build: docker-build
	docker run --user=$(shell id -u) -ti --rm --name cloudify-tpn-plugin -v /etc/passwd:/etc/passwd -v "${PWD}":/usr/src/app -w /usr/src/app cloudify-tpn-plugin python setup.py build

test: build
	docker run --user=$(shell id -u) -ti --rm --name cloudify-tpn-plugin -v /etc/passwd:/etc/passwd -v "${PWD}":/usr/src/app -w /usr/src/app cloudify-tpn-plugin tox -e py27,flake8
	#	docker run --user=$(shell id -u) -ti --rm --name cloudify-tpn-plugin -v /etc/passwd:/etc/passwd -v "${PWD}":/usr/src/app -w /usr/src/app cloudify-tpn-plugin py.test

package: build
	docker run --user=$(shell id -u) -ti --rm --name cloudify-tpn-plugin -v /etc/passwd:/etc/passwd -v "${PWD}":/usr/src/app -w /usr/src/app cloudify-tpn-plugin wagon create cloudify-tpn-plugin.zip -v -f -a '--no-cache-dir -c constraints.txt'

zip:
	cd ..; zip -r $T/cloudify-tpn-plugin.zip $T/cloudify_tpn $T/constraints.txt $T/dev-requirements.txt $T/LICENSE $T/plugin.yaml $T/README.md $T/setup.py $T/tox.ini
	tar czf tpn_test_blueprint.tar.gz tpn_test_blueprint/

publish: build zip
	cp tpn_test_blueprint.tar.gz cloudify-tpn-plugin.zip *.wgn plugin.yaml /data/main/www/apache/htdocs/cloudify
